This repo contains the work Samy Coulombe completed in the period since
he was added to the project but before he had access to the official 
repositories.

This repo's contents should be merged into said official repos as soon
as Samy gains access.