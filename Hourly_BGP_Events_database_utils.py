# NOTE: This is just a first draft; tests should be removed
#       before using this in production

# standard library dependencies
import os
import time
import doctest
import sqlite3
from datetime import datetime, timezone

# external dependencies
import numpy as np
import pandas as pd

def create_table(database_filepath: str) -> None:
    """Creates the `Hourly_BGP_Events` table at the specified
    file path. The table's schema is hard-coded to follow the
    one specified in the report entitled 
    `Ciena Data Science Report 2`

    Parameters
    ----------
    database_filepath : str
        Filepath indicating where the database will be saved.

    Returns
    -------
    None
    
    Examples
    --------
    >>> create_table("temp.db")
    >>> valid_sqlite3_types = sqlite3.PARSE_DECLTYPES | sqlite3.PARSE_COLNAMES
    >>> with sqlite3.connect("temp.db", detect_types=valid_sqlite3_types) as connection:
    ...     cursor = connection.cursor()
    ...     _ = cursor.execute("SELECT * FROM Hourly_BGP_Events LIMIT 1;")
    ...     columns = [tup[0] for tup in cursor.description]
    ...     assert columns == ['Time', 'Router', 'Event_Operation', 'Operand', 'Attributes', 'Area_or_AS', 'AS_Path', 'AS4_Path', 'Local_Pref', 'MED', 'Communities', 'MP_Reachability_Next_Hop', 'Next_Hop', 'Originator_ID', 'Cluster_List', 'Aggregator', 'Atomic', 'Aggregator_AS4', 'Ext_Communities', 'P_Tunnel', 'Path_ID']
    ...     cursor.close() 

    """
    
    with sqlite3.connect(database_filepath, detect_types=sqlite3.PARSE_DECLTYPES | sqlite3.PARSE_COLNAMES) as connection:
        cursor = connection.cursor()
        table_creation_query = """CREATE TABLE Hourly_BGP_Events(
            Time                        TIMESTAMP,
            Router                      TEXT,
            Event_Operation             TEXT,
            Operand                     TEXT,
            Attributes                  TEXT,
            Area_or_AS                  TEXT,
            AS_Path                     TEXT,
            AS4_Path                    REAL,
            Local_Pref                  REAL,
            MED                         REAL,
            Communities                 TEXT,
            MP_Reachability_Next_Hop    TEXT,
            Next_Hop                    TEXT,
            Originator_ID               TEXT,
            Cluster_List                TEXT,
            Aggregator                  TEXT,
            Atomic                      REAL,
            Aggregator_AS4              REAL,
            Ext_Communities             REAL,
            P_Tunnel                    REAL,
            Path_ID                     REAL
        );
        """
        cursor.execute(table_creation_query)
        connection.commit()
        cursor.close()
    
def populate_table_from_df( database_filepath: str, 
                            df: pd.DataFrame) -> None:
    """
    Populates the `Hourly_BGP_Events` table at the specified filepath
    with the values contained in the provided dataframe's pertinent
    columns.

    Parameters
    ----------
    database_filepath : str
        Filepath indicating where the database will be saved.
    df : pd.DataFrame
        Pandas DataFrame whose values will be inserted into the `Hourly_BGP_Events` table.

    Returns
    -------
    None
    """
    df_copy = df[["Time", "Router", "Event_Operation",
                  "Operand", "Attributes", "Area_or_AS",
                  "AS_Path", "AS4_Path", "Local_Pref",
                  "MED", "Communities", "MP_Reachability_Next_Hop",
                  "Next_Hop", "Originator_ID", "Cluster_List",
                  "Aggregator", "Atomic", "Aggregator_AS4",
                  "Ext_Communities", "P_Tunnel", "Path_ID"                
                ]]

    records_to_insert = list(map(tuple, df_copy.values))
    insertion_query = """INSERT INTO Hourly_BGP_Events 
    VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);
    """
    
    with sqlite3.connect(database_filepath, detect_types=sqlite3.PARSE_DECLTYPES | sqlite3.PARSE_COLNAMES) as connection:
        cursor = connection.cursor()
        cursor.executemany(
            insertion_query,
            records_to_insert
        )
        connection.commit()
        cursor.close()
    connection.close()

def test_creation_insertion_and_querying(database_filepath: str):
    """
    Basic function to test 
    1. creating a database
    2. inserting values into the database
    3. reading from the database
    4. querying the database with a date range
    """
    create_table(database_filepath)
    random_df = pd.DataFrame(
        np.random.random((5,21)),
        columns = ["Time", "Router", "Event_Operation",
                  "Operand", "Attributes", "Area_or_AS",
                  "AS_Path", "AS4_Path", "Local_Pref",
                  "MED", "Communities", "MP_Reachability_Next_Hop",
                  "Next_Hop", "Originator_ID", "Cluster_List",
                  "Aggregator", "Atomic", "Aggregator_AS4",
                  "Ext_Communities", "P_Tunnel", "Path_ID"                
                ]
    )
    for i in range(5):
        time.sleep(1)
        random_df.iloc[i,0] = datetime.now(timezone.utc)
    
    random_df = random_df.astype({
        "Time": 'str',
        "Router": 'str',
        "Event_Operation": 'str', 
        "Operand": 'str', 
        "Attributes": 'str', 
        "Area_or_AS": 'str', 
        "AS_Path": 'str',
        "Communities": 'str', 
        "MP_Reachability_Next_Hop": 'str', 
        "Next_Hop": 'str', 
        "Originator_ID": 'str', 
        "Cluster_List": 'str',
        "Aggregator": 'str'
    })
    
    populate_table_from_df(database_filepath, random_df)
    with sqlite3.connect("temp.db", detect_types=sqlite3.PARSE_DECLTYPES | sqlite3.PARSE_COLNAMES) as connection:
        cursor = connection.cursor()
        df_from_database = pd.DataFrame(
            cursor.execute("SELECT * FROM Hourly_BGP_Events;").fetchall()
        )
        random_df.Time = random_df.Time.astype('datetime64')
        try:
            assert (random_df.values == df_from_database.values).all()
        except AssertionError as ae:
            breakpoint()
            raise ae
        else:
            print("✓ Passed Creation and Insertion test.")
        cursor.close()
    connection.commit()
    connection.close()

    with sqlite3.connect("temp.db", detect_types=sqlite3.PARSE_DECLTYPES | sqlite3.PARSE_COLNAMES) as connection:
        cursor = connection.cursor()
        df_from_database = pd.DataFrame(
            cursor.execute(f"""SELECT * FROM Hourly_BGP_Events
            WHERE Time BETWEEN '{random_df.iloc[1,0]}' AND '{random_df.iloc[-2,0]}'""").fetchall()
        )
        random_df.Time = random_df.Time.astype('datetime64')
        try:
            assert (random_df.iloc[1:-2, :].values == df_from_database.values).all()
        except AssertionError as ae:
            breakpoint()
            raise ae
        else:
            print("✓ Passed Date Range Selection test.")
        cursor.close()
    connection.commit()
    connection.close()
    
def run_basic_tests():
    """Wrapper function to execute and clean up after `test_creation_insertion_and_querying`"""
    doctest.testmod()
    os.remove("temp.db")
    test_creation_insertion_and_querying("temp.db")
    os.remove("temp.db")
    

if __name__ == '__main__':
    run_basic_tests()
    